{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
--{-# OPTIONS_GHC -fno-warn-unused-top-binds #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import ClassyPrelude

import qualified Data.Vector.Storable as Vector
import qualified System.Random        as Random

import qualified Gltf
import qualified Gltf.Primitive

main :: IO ()
main = do
  putStrLn "gltf example: main..."
  --putStrLn $ tshow $ Gltf.to_bytes Gltf.hello_triangle
  writeFile "example.gltf" $ snd . headEx $ Gltf.to_bytes
    $ Gltf.hello_triangle Gltf.GltfBase64
  putStrLn "wrote \"example.gltf\""
  rng <- Random.getStdGen
  writeFile "random.gltf" $ snd . headEx $ Gltf.to_bytes
    $ Gltf.primitive_position3_float Gltf.GltfBase64 Gltf.Primitive.Triangles
      $ Vector.fromList $ take (3 * 3 * 100) $ Random.randoms rng
  putStrLn "wrote \"random.gltf\""
  putStrLn "gltf example: ...main"
