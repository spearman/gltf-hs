{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE DeriveGeneric #-}

module Gltf.Primitive where

import BasicPrelude

import GHC.Generics (Generic)
import Text.Read    (Read)
import Text.Show    (Show)

import qualified Data.Aeson as Aeson

import qualified Gltf.Util  as Util

data Mode
  = Points
  | Lines
  | LineLoop
  | LineStrip
  | Triangles
  | TriangleStrip
  | TriangleFan
  deriving (Enum, Generic, Read, Show)
instance Aeson.ToJSON   Mode where
  toJSON    = Aeson.genericToJSON    Util.json_options
instance Aeson.FromJSON Mode where
  parseJSON = Aeson.genericParseJSON Util.json_options
