{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE DeriveGeneric #-}

module Gltf.Json.Scene where

import BasicPrelude

import GHC.Generics (Generic)
import Text.Read    (Read)
import Text.Show    (Show)

import Data.Default (Default)
import Data.Default.Instances.Text()

import qualified Data.Aeson as Aeson

import qualified Gltf.Util  as Util

data Scene = Scene {
  nodes :: [Word32]
} deriving (Read, Show, Generic)
instance Aeson.ToJSON   Scene where
  toJSON    = Aeson.genericToJSON    Util.json_options
instance Aeson.FromJSON Scene where
  parseJSON = Aeson.genericParseJSON Util.json_options
instance Default Scene
