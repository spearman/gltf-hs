{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Gltf where

import BasicPrelude
import GHC.Generics (Generic)
import Text.Read    (Read)
import Text.Show    (Show)

import Data.Default    (Default (def))

import qualified Data.Aeson             as Aeson
import qualified Data.ByteString        as ByteString
import qualified Data.ByteString.Base64 as ByteString.Base64
import qualified Data.ByteString.Lazy   as ByteString.Lazy
import qualified Data.Map.Strict        as Map
import qualified Data.Vector.Storable   as Vector
import qualified Data.Vector.Storable.ByteString as Vector.ByteString

import qualified Gltf.Accessor    as Accessor
import qualified Gltf.Json        as Json
import qualified Gltf.Json.Scene  as Json.Scene
import qualified Gltf.Json.Buffer as Json.Buffer
import qualified Gltf.Primitive   as Primitive
import qualified Gltf.Util        as Util

data Gltf = Gltf Format Json.Json deriving (Read, Show, Generic)
instance Default Gltf

data Format
  = GltfBase64
  | GltfBin
  | Glb
  deriving (Read, Show, Generic)
instance Aeson.ToJSON   Format where
  toJSON    = Aeson.genericToJSON    Util.json_options
instance Aeson.FromJSON Format where
  parseJSON = Aeson.genericParseJSON Util.json_options
instance Default Format where
  def = GltfBase64

data FileType
  = Json
  | Buffer
  | Image Image
  | Binary
  deriving (Read, Show)

data Image
  = Bmp
  | Png
  deriving (Read, Show)

to_bytes :: Gltf -> [(FileType, ByteString)]
to_bytes (Gltf GltfBase64 json) =
  [(Json, ByteString.Lazy.toStrict . Aeson.encode $ json)]
to_bytes (Gltf GltfBin _json)   = undefined
to_bytes (Gltf Glb _json)       = undefined

hello_triangle :: Format -> Gltf
hello_triangle format = primitive_position3_float format Primitive.Triangles $
  Vector.fromList [
    -2.0, -2.0, 0.0,
     2.0, -2.0, 0.0,
     0.0,  2.0, 0.0 ]

primitive_position3_float :: Format -> Primitive.Mode -> Vector.Vector Float
  -> Gltf
primitive_position3_float format mode positions = Gltf format $ def {
  Json.scenes = [ def { Json.Scene.nodes = [0] } ],
  Json.nodes  = [ def ],
  Json.meshes = [ def {
    Json.primitives = [ def {
      Json.attributes = Map.fromList [("POSITION", 0)],
      Json.mode       = fromIntegral $ fromEnum mode
    } ]
  } ],
  Json.accessors = [ def {
    Json.count = fromIntegral count,
    Json.type_ = "VEC3",
    Json.componentType = Accessor.component_type_float,
    Json.min   = map Vector.minimum [xs, ys, zs],
    Json.max   = map Vector.maximum [xs, ys, zs]
  } ],
  Json.bufferViews = [ def {
    Json.byteLength = byte_length
  } ],
  Json.buffers = [ def {
    Json.Buffer.uri = case format of
      GltfBase64 -> Just $
        "data:application/octet-stream;base64," ++ decodeUtf8 base64
      GltfBin    -> undefined   -- TODO
      Glb        -> undefined,  -- TODO
    Json.Buffer.byteLength = byte_length
  } ]
} where
    byte_length = fromIntegral $ ByteString.length bytes :: Word32
    bytes       = Vector.ByteString.vectorToByteString positions
    base64      = ByteString.Base64.encode bytes
    count       = Vector.length positions `div` 3
    xs          = skip_each 0 3
    ys          = skip_each 1 3
    zs          = skip_each 2 3
    skip_each :: Int -> Int -> Vector.Vector Float
    skip_each m n = Vector.generate count (\i -> positions Vector.! (m + i * n))
      
