{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module Gltf.Accessor where

import BasicPrelude

import Data.Word (Word32)
import Text.Read (Read)
import Text.Show (Show)

data ComponentType
  = Byte
  | UnsignedByte
  | Short
  | UnsignedShort
  | UnsignedInt
  | Float
  deriving (Read, Show)

instance Enum ComponentType where
  toEnum int = case int of
    5120 -> Byte
    5121 -> UnsignedByte
    5122 -> Short
    5123 -> UnsignedShort
    5125 -> UnsignedInt
    5126 -> Float
    _ -> error $ "invalid component type: " ++ show int
  fromEnum component_type = fromIntegral $ case component_type of
    Byte          -> component_type_byte          
    UnsignedByte  -> component_type_unsigned_byte 
    Short         -> component_type_short         
    UnsignedShort -> component_type_unsigned_short
    UnsignedInt   -> component_type_unsigned_int  
    Float         -> component_type_float         

component_type_byte           = 5120 :: Word32
component_type_unsigned_byte  = 5121 :: Word32
component_type_short          = 5122 :: Word32
component_type_unsigned_short = 5123 :: Word32
component_type_unsigned_int   = 5125 :: Word32
component_type_float          = 5126 :: Word32
