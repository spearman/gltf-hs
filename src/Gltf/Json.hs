{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}

module Gltf.Json where

import BasicPrelude

import GHC.Generics (Generic)
import Text.Read    (Read)
import Text.Show    (Show)

import Data.Default (Default (def))
import Data.Default.Instances.Text()
import Data.Map.Strict (Map)

import qualified Data.Aeson as Aeson

import Gltf.Json.Buffer (Buffer)
import Gltf.Json.Scene  (Scene)
import qualified Gltf.Util as Util

data Json = Json {
  asset        :: Asset,
  scenes       :: [Scene],
  nodes        :: [Node],
  meshes       :: [Mesh],
  accessors    :: [Accessor],
  bufferViews  :: [BufferView],
  buffers      :: [Buffer]
} deriving (Read, Show, Generic)
instance Aeson.ToJSON   Json where
  toJSON    = Aeson.genericToJSON    Util.json_options
instance Aeson.FromJSON Json where
  parseJSON = Aeson.genericParseJSON Util.json_options
instance Default Json

data Asset = Asset {
  version :: Text
} deriving (Read, Show, Generic)
instance Aeson.ToJSON   Asset where
  toJSON    = Aeson.genericToJSON    Util.json_options
instance Aeson.FromJSON Asset where
  parseJSON = Aeson.genericParseJSON Util.json_options
instance Default Asset where
  def = Asset "2.0"

data Node = Node {
  mesh :: Word32
} deriving (Read, Show, Generic)
instance Aeson.ToJSON   Node where
  toJSON    = Aeson.genericToJSON    Util.json_options
instance Aeson.FromJSON Node where
  parseJSON = Aeson.genericParseJSON Util.json_options
instance Default Node

data Mesh = Mesh {
  primitives :: [Primitive]
} deriving (Read, Show, Generic)
instance Aeson.ToJSON   Mesh where
  toJSON    = Aeson.genericToJSON    Util.json_options
instance Aeson.FromJSON Mesh where
  parseJSON = Aeson.genericParseJSON Util.json_options
instance Default Mesh

data Primitive = Primitive {
  attributes :: Map Text Word32,
  mode       :: Word32
} deriving (Read, Show, Generic)
instance Aeson.ToJSON   Primitive where
  toJSON    = Aeson.genericToJSON    Util.json_options
instance Aeson.FromJSON Primitive where
  parseJSON = Aeson.genericParseJSON Util.json_options
instance Default Primitive

data Accessor = Accessor {
  bufferView    :: Word32,
  count         :: Word32,
  type_         :: Text,
  componentType :: Word32,
  min           :: [Float],
  max           :: [Float]
} deriving (Read, Show, Generic)
instance Aeson.ToJSON   Accessor where
  toJSON    = Aeson.genericToJSON    Util.json_options {
    Aeson.fieldLabelModifier = (\s -> case s of
      "type_" -> "type"
      _       -> s)
  }
instance Aeson.FromJSON Accessor where
  parseJSON = Aeson.genericParseJSON Util.json_options
instance Default Accessor

data BufferView = BufferView {
  buffer     :: Word32,
  byteLength :: Word32
} deriving (Read, Show, Generic)
instance Aeson.ToJSON   BufferView where
  toJSON    = Aeson.genericToJSON    Util.json_options
instance Aeson.FromJSON BufferView where
  parseJSON = Aeson.genericParseJSON Util.json_options
instance Default BufferView
