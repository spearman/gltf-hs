{-# OPTIONS_GHC -Wall #-}

module Gltf.Util where

import BasicPrelude

import qualified Data.Aeson as Aeson

json_options :: Aeson.Options
json_options = Aeson.defaultOptions {
  Aeson.omitNothingFields = True,
  Aeson.sumEncoding       = Aeson.ObjectWithSingleField
}
