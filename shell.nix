{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, aeson, base, base64-bytestring, basic-prelude
      , bytestring, bytestring-to-vector, classy-prelude, containers
      , data-default, data-default-instances-text, mono-traversable
      , random, stdenv, vector
      }:
      mkDerivation {
        pname = "gltf";
        version = "0.1.0.0";
        src = ./.;
        isLibrary = true;
        isExecutable = true;
        libraryHaskellDepends = [
          aeson base base64-bytestring basic-prelude bytestring
          bytestring-to-vector containers data-default
          data-default-instances-text mono-traversable vector
        ] ++ mydevtools;
        executableHaskellDepends = [ base classy-prelude random vector ]
          ++ mydevtools;
        homepage = "https://gitlab.com/spearman/gltf-hs.git";
        license = stdenv.lib.licenses.asl20;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

  mydevtools = [
    haskellPackages.ghc
    haskellPackages.cabal-install
    haskellPackages.profiterole
    haskellPackages.profiteur
    haskellPackages.stack
    # see cachix + hercules-ci setup in configuration.nix to avoid long
    # compilation process!
    (import (builtins.fetchTarball
      "https://github.com/hercules-ci/ghcide-nix/tarball/master") {}
    ).ghcide-ghc865
  ];

in

  if pkgs.lib.inNixShell then drv.env else drv
