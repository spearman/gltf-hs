{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE DeriveGeneric #-}

module Gltf.Json.Buffer where

import BasicPrelude

import GHC.Generics (Generic)
import Text.Read    (Read)
import Text.Show    (Show)

import Data.Default (Default)
import Data.Default.Instances.Text()

import qualified Data.Aeson as Aeson

import qualified Gltf.Util  as Util

data Buffer = Buffer {
  uri        :: Maybe Text,
  byteLength :: Word32
} deriving (Read, Show, Generic)
instance Aeson.ToJSON   Buffer where
  toJSON    = Aeson.genericToJSON    Util.json_options
instance Aeson.FromJSON Buffer where
  parseJSON = Aeson.genericParseJSON Util.json_options
instance Default Buffer
